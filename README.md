Classifying queries submitted to a vertical search engine
=========================================================

Documentation, and feature files and code used to reproduce experiments from
the paper:

Berendsen, R., Kovachev, B., Meij, E., De Rijke, M., & Weerkamp, W. (2011,
June). Classifying queries submitted to a vertical search engine. In
Proceedings of the 3rd International Web Science Conference (p. 4). ACM.

The most up to date report on these experiments can be found in Chapter three
of the PhD dissertation:

Finding people, papers and posts: domain-specific algorithms and evaluation,
Berendsen, R., 2015.

Reproducing the experiments
---------------------------
First locate the feature files. These files, with the original feature values,
can be found in `./exp`, they have an extension .arff, the standard format of
feature files for Weka. 

Then, install Weka. We used Weka 3.6.4 to run the experiments, you can use
newer versions, of course, though the results may differ a bit (hopefully for
the better ;-)).

Add your weka.jar to your classpath if you, like me, get tired
of using `-cp` with each `java` call. Assuming you have one weka.jar on
your machine, something like this may work, on a Unix-like machine:

    export CLASSPATH="$( locate weka.jar ):$CLASSPATH"

Now you're all set. Follow the steps below to run a classifier and
output predictions, compute performance scores, and perform significance
tests.

### Running classifiers with Weka 
The key thing about this section is that we will give each data instance
with an ID attribute, then tell Weka to ignore it during classification,
and then let Weka print the idea again along with the predictions. This
will allow us to do some error analysis, perform statistical tests, and
so on, without having to do it all in the Weka software package itself.

We will let Weka perform cross validation in this section. If you would
like to do the cross validation yourself also, take a look at the
`README.md` file in `./src/cv/`. Okay, let's get started.

First, add an ID attribute as the first attribute in the high_low.arff
file. Weka can do this for you, or you can do it by hand almost as
easily (if you use Vim). Now, our classifier has to ignore this
attribute, and print it out later again, along with the predictions.
A way to do this is to apply a FilteredClassifier first, which applies a
filter to the dataset before running a specified classifier. We will
apply the Remove filter, to remove the first feature. 

    java weka.classifiers.meta.FilteredClassifier -t \
    high_low_with_ids.arff -p 1  -F \
    "weka.filters.unsupervised.attribute.Remove -R 1" -W \
    weka.classifiers.trees.J48 -- -C 0.25 -M 2 -Q 1

Notice how we quoted the argument to the `-F` option for the
`FilteredClassifier` class. This is a trick that works for the -F
attribute, but, unfortunately, it does not seem to work for the `-W`
attribute. For that attribute, we use a trick with the special option
`--`. Arguments after this option are correctly considered as belonging
to the `J48` class. This latter trick I did not get to work for the `-F`
option. For more information on this tricky topic (and a confession that
it's messy and that there is no single strategy that works for every
class), see https://weka.wikispaces.com/Primer, near the bottom of that
page, in the discussion of some examples.

So now, let's try and run a different classifier, and see if the data
points are shuffled in exactly the same way. If so, that means Weka uses
the exact same folds for training and testing the next classifier. And
that would mean we can say that for each testing point, two classifiers
will have seen the same training points. This is important, because if
they would have seen different training points, even the same classifier
may have different performance on the same testing point.

    java weka.classifiers.meta.FilteredClassifier -t \
    high_low_with_ids.arff -p 1  -F \
    "weka.filters.unsupervised.attribute.Remove -R 1" -W \
    weka.classifiers.bayes.NaiveBayes

Indeed, as we would expect, the ordering is the same. So, the
FilteredClassifier must use a seed. The last classifier we use
is the SVM implementation in Weka:

    java weka.classifiers.meta.FilteredClassifier -t \
    high_low_with_ids.arff -p 1 -F \
    "weka.filters.unsupervised.attribute.Remove -R 1" -W \
    weka.classifiers.functions.SMO -- -C 1.0 -L 0.0010 -P 1.0E-12 \
    -N 0 -V -1 -W 1 

Notice how the SMO classifier also uses a seed value of 1 (via the `-W` option,
this time). Perhaps the FilteredClassifier uses the seed of the classifier it
employs to create the folds, and this is why all classifiers have the same
fold? Indeed, running this one, we get the same folds again, it seems at first
sight.

Great! That means we get exactly the same results as in the original
paper, but now we'll be able to compute significance tests for the
two-way experiment, because we have the results for each individual data
point. But first, let's compute some performance scores in the first
place. 

### Computing performance scores
The script `./src/parse_weka_predictions.py` parses the weka predictions
we have just generated. Call it like this:

    python ./src/parse_weka_predictions.py < predictions

And it will print the result to stdout, a tab separated listing, with an
instance id, the truth label, and the predicted label on each line.
After saving these files with parsed predictions, e.g., to a file named
`J48_two_way.predictions`, you can compute precision, recall, F-score
(harmonic mean with precision and recall weighted equally), and the
number of instances in each class with the script call

    pythn ./src/compute_metrics < J48_two_way.predictions

The output is not very beautifully formatted, but you should be able to
figure out what is what pretty easily (just take one look at the code).
You can also compare the outcomes to the outcomes Weka itself gives when
you supply the `-i` option instead of the `-p` option.  The values for
precision, recall and F-score should match exactly. There is just one
addition in our script, we compute a slightly different version of the
macro-averaged F-score for each classifier, namely the version in this
highly cited and quite systematic paper on evaluation metrics:

Sokolova, M., & Lapalme, G. (2009). A systematic analysis of performance
measures for classification tasks. Information Processing & Management,
45(4), 427-437.

Besides, now that we calculate the metrics in a Python script (using
scikit-learn), we can conveniently calculate a significance test in
Python, too. Before we look at that, you can run the three classifiers
in exactly the same way on the dataset that has three kinds of labels.
You should get the same results as in our paper.

### Running significance tests
The test we will run is a two-tailed Fisher's pairwise randomization test. We
follow the description of the algorithm given in this paper:

Smucker, M. D., Allan, J., & Carterette, B. (2007, November). A comparison of
statistical significance tests for information retrieval evaluation. In
Proceedings of the sixteenth ACM conference on Conference on information and
knowledge management (pp. 623-632). ACM.

The nice thing about the test is that it is non-parametric. It makes no
assumptions about the shape of the distribution over instances that the metric
we test on has. Instead, in the test the null hypothesis states, informally,
that we have not run two different systems on the instances. Instead, we have
run a single system twice on each instance, and we have labeled one of the
outputs as belonging to system 'A', and the other as belonging to system 'B'.
So, according to this hypothesis, it does not matter which choice we make.  We
can generate, say, a hundred thousand random labellings, and compute for each
labeling the difference in performance between system 'A' and system 'B'. If we
plot these differences, we should see something that looks like a normal
distribution centered around zero. But if the null hypothesis was true, then
what is the chance that a performance difference at least as large as the one
we observed in our experiment occurs? This probability can be estimated using
the proportion of randomly sampled labellings that has a difference (the
absolute value of which is)  at least this large. With the script call

    python ./src/rt.py A.predictions B.predictions iterations out_dir

where `A.predictions` and `B.predictions` are the files produced by the script
`parse_weka_predictions.py` (see above), you will get this estimated p-value,
based on the number of `iterations` you require. Histograms of observed random
differences are saved in `out_dir`, which the script attempts to create.
Because we can compute this p-value for any metric, we will, just for the fun
of it. So we output p-values for precision, recall and f-score for each class,
and also for the averaged scores.  If you would want to do this really well,
you should perhaps run the script once for each metric, but perhaps that is a
detail. For 10000 iterations, the script takes under a minute on a single core
machine. 100000 iterations would be better, and I expect you would need ten
minutes for that; the algorithm is linear in complexity. Smucker et al
also report on experiments with in the order of a million iterations,
and note that p-values do not differ much. A paper discussing this
randomization test in the context of classification is:

Yeh, A. (2000, July). More accurate tests for the statistical
significance of result differences. In Proceedings of the 18th
conference on Computational linguistics-Volume 2 (pp. 947-953).
Association for Computational Linguistics.

He details an example dataset with example predictions for two methods.
In this repository you can find the two prediction sets for these two
methods, so you can run `rt.py` and compare p-values with those in that
paper (note that he does a one-tailed test, and we do a two-tailed test,
so multiply by two). See also the README.md file in the directory
`./src/toy_examples_rt/yeh2000more/`.

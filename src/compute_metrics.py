"""
Usage: compute_metrics < predictions
"""
import sys
from sklearn.metrics import precision_recall_fscore_support

def main(argv):

    if len(argv) != 1:
        print >> sys.stderr, __doc__
        return 64

    y_true = []
    y_pred = []

    for l in sys.stdin:
        _, truth, predicted = l.strip().split()
        y_true.append(truth)
        y_pred.append(predicted)

    precision, recall, f_score, support = \
            precision_recall_fscore_support(y_true,
                    y_pred, 1.0)

    print "Precision, recall, F score, and support for all classes in"
    print "the data (ordered by their labels)"
    print precision, recall, f_score, support

    print "Macro-averaged precision, recall, and F1-score"
    print "Note that these just are the averages of\nthe values for each individual class."
    avg_p, avg_r, avg_f, avg_s = \
            precision_recall_fscore_support(y_true,
            y_pred, beta=1.0, pos_label=None, average='macro')
    print avg_p, avg_r, avg_f, avg_s

    print "Now the F-score computed with the macro-averaged precision"
    print "and recall scores, as in Sokolova's paper"
    f_M = 2 * avg_p * avg_r / (avg_p + avg_r)
    print f_M

    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))

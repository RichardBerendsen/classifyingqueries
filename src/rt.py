"""
Usage: rt.py systemA.predictions systemB.predictions iterations out_dir

Will output estimated p-value for a two-tailed Fisher pairwise randomization
test, according to Smucker et al, 2007, for a variety of metrics for a 
multi-class classification task.
"""
import sys
import os
import random
from matplotlib.pyplot import hist, savefig, figure
from sklearn.metrics import precision_recall_fscore_support

def score(truth, predictions, labels):
    p, r, f, _ = precision_recall_fscore_support(truth, predictions, beta=1.0, labels=labels)
    avg_p, avg_r, avg_f, _ = \
            precision_recall_fscore_support(truth, predictions, beta=1.0, labels=labels,
            pos_label=None, average='macro')
    f_M = 2 * avg_p * avg_r / (avg_p + avg_r)
    metrics = [i for l in [p, r, f] for i in l]
    metrics.extend([avg_p, avg_r, avg_f, f_M])
    return metrics

def random_diffs(truth, a_b, labels):
    a_rand = []
    b_rand = []
    for a, b in a_b:
        # don't swap labels if they are the same anyway, this uses up part of a
        # period of the random generator for nothing.
        if a != b:
            # flip a fair coin to decide the labeling.
            if random.random() < 0.5: # do not swap
                a_rand.append(a)
                b_rand.append(b)
            else: # swap
                a_rand.append(b)
                b_rand.append(a)
        else: # do not swap (or swap, equivalently)
            a_rand.append(a)
            b_rand.append(b)
    scores_a_rand = score(truth, a_rand, labels)
    scores_b_rand = score(truth, b_rand, labels)
    return [b - a for a, b in zip(scores_a_rand, scores_b_rand)]

def randomization_test(truth, a_b, labels, out_dir, iterations):
    rand_diffs_per_metric = zip(*[random_diffs(truth, a_b, labels) for i in range(iterations)])
    metric_labels = ['P-%s' % (l,) for l in labels]
    metric_labels.extend(['R-%s' % (l,) for l in labels])
    metric_labels.extend(['F-%s' % (l,) for l in labels])
    metric_labels.extend(['Avg-P', 'Avg-R', 'Avg-F', 'F_M'])
    os.mkdir(out_dir)
    for rand_diffs, metric_label in zip(rand_diffs_per_metric, metric_labels):
        figure()
        hist(rand_diffs)
        savefig('%s/%s_random_diffs.pdf' % (out_dir, metric_label,))
    a, b = zip(*a_b)
    scores_a = score(truth, a, labels)
    scores_b = score(truth, b, labels)
    observed_diffs = [score_b - score_a for score_a, score_b \
            in zip(scores_a, scores_b)]
    for rand_diffs, observed_diff, metric_label, score_a, score_b \
            in zip(rand_diffs_per_metric, observed_diffs, metric_labels,
                    scores_a, scores_b):
        p_value = float(sum([abs(rd) >= abs(observed_diff) for rd in rand_diffs]))/iterations
        print "%s: %.6f (observed scores: %.4f, %.4f)" % (metric_label, p_value, score_a, score_b)

def load_predictions(path_a, path_b):
    predictions = {}
    # predictions[instance_id] holds a list [a, b] with predictions of system
    # a and b.
    labels = set([])

    f_a = open(path_a)
    f_b = open(path_b)
    pred_a = []
    for l in f_a:
        id, truth, label_a = l.strip().split()
        labels.add(truth)
        if id in predictions:
            print >> sys.stderr, "Duplicate id in predictions system A"
            return 2
        predictions[id] = [truth, label_a]
    labels = sorted(labels)
    for l in f_b:
        id, truth, label_b = l.strip().split()
        if not id in predictions:
            print >> sys.stderr, "Missing prediction for system A"
            return 2
        if not predictions[id][0] == truth:
            print >> sys.stderr, "Truth labels system A and B disagree"
            return 2
        if len(predictions[id]) == 3:
            print >> sys.stderr, "Duplicate id in predictions system B"
            return 2
        predictions[id].append(label_b)
    for k, v in predictions.iteritems():
        if len(v) != 3:
            print >> sys.stderr, "Missing prediction for system B"
            return 2
    f_a.close()
    f_b.close()
    return predictions, labels

def main(argv):

    if len(argv) != 5:
        print >> sys.stderr, __doc__
        return 64

    predictions, labels = load_predictions(argv[1], argv[2])
    iterations = int(argv[3])
    out_dir = argv[4]


    truth, a, b = zip(*predictions.values())
    a_b = zip(a, b)

    randomization_test(truth, a_b, labels, out_dir, iterations)

    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))

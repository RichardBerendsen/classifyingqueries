"""
Usage: concat_folds.py predictions_dir out_dir

Will create out_dir and put two files in there: 
1. test.class_labels
2. predictions
"""
import sys
import os

def main(argv):

    if len(argv) != 3:
        print >> sys.stderr, __doc__
        return 64

    path_predictions_dir = argv[1]
    path_out_dir = argv[2]

    folds_predictions_dir = frozenset(
            [int(i) for i in os.listdir(path_predictions_dir)])

    # open output files
    os.mkdir(path_out_dir)
    f_out_test_class_labels = open(os.path.join(path_out_dir,
            'test.class_labels'), 'w')
    f_out_predictions = open(os.path.join(path_out_dir, 'predictions'), 'w')

    folds = sorted(folds_predictions_dir)
    for f in folds:
        f_test_class_labels = open(os.path.join(path_predictions_dir,
                '%d' % (f,), 'test.class_labels'))
        f_predictions = open(os.path.join(path_predictions_dir,
                '%d' % (f,), 'predictions'))

        for l in f_test_class_labels:
            f_out_test_class_labels.write(l)

        for l in f_predictions:
            f_out_predictions.write(l)

        f_test_class_labels.close()
        f_predictions.close()

    f_out_test_class_labels.close()
    f_out_predictions.close()

    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))

"""
Usage: compute_metrics test.class_labels predictions
"""
import sys
from sklearn.metrics import precision_recall_fscore_support

def main(argv):

    if len(argv) != 3:
        print >> sys.stderr, __doc__
        return 64

    f_test_class_labels = open(argv[1])
    f_predictions = open(argv[2])

    y_true = [l.strip() for l in f_test_class_labels]
    y_pred = [l.strip() for l in f_predictions]

    precision, recall, f_score, support = \
            precision_recall_fscore_support(y_true,
                    y_pred, 1.0,)

    print precision, recall, f_score, support

    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))

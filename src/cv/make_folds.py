"""
make_folds.py seed n_folds features.arff out_dir

Will create directory out_dir, and create `n_folds` stratified folds in
out_dir/1, out_dir/2, etc, from features.arff. Requires a seed (integer)
as the first argument.
"""
import sys
import os
import random

"""
Parses a feature file in .arff format.
Simply separate in header (string) and features (list of lists of
strings).
"""
def parse_arff(path):

    f = open(path)
    header = []
    data = []

    # Read and add to header until after encountering '@data' on a line
    # by itself
    while True:
        l = f.readline()
        if l == '': # eol
            break
        header.append(l)
        if l == '@data\n':
            break

    # Assume the rest is data. Split by ','
    while True:
        l = f.readline()
        if l == '': # eol
            break
        data.append(tuple(l.strip().split(',')))
            # Note that we created a tuple.

    f.close()
    return header, data

"""
Create ten stratified folds in path_out_dir.
"""
def create_folds(seed, data, n_folds):

    # d['class'] will hold `data` indices for class 'class'.
    d = {}

    # f[i] will hold indices of `data` belonging to fold i.
    f = [[] for i in range(n_folds)]

    # Assume last field of tuples (x[-1]) in data is class label.

    # Store `data` indices in d, separately for each class. 
    for data_i, x in enumerate(data):
        if x[-1] in d:
            d[x[-1]].append(data_i)
        else:
            d[x[-1]] = [data_i]

    # Initialize the random number generator
    if seed != 0:
        random.seed(seed)
    # For each class:
    #   sort indices in d for each class randomly, in place,
    #   walk through data, and assign each point to the
    #   next fold, until there are no more points.
    for k, v in d.iteritems():
        if seed != 0:
            random.shuffle(v)
        for fold_i, data_i in enumerate(v):
            f[fold_i % n_folds].append(data_i)

    return f

"""
Serialize folds.
"""
def serialize_folds(header, data, folds, path_out_dir):

    # Create output directory.
    # Will fail if dir exists, or if user has no permission to create.
    os.mkdir(path_out_dir)

    # Write out the folds
    for i, f in enumerate(folds):
        dir_out = os.path.join(path_out_dir, '%d' % (i + 1,))
        os.mkdir(dir_out)

        # Write test file.
        path_test = os.path.join(dir_out, 'test.arff')
        f_test = open(path_test, 'w')
        for l in header:
            f_test.write(l)
        for data_i in f:
            print >> f_test, ','.join(data[data_i])
        f_test.close()

        # Write train file, with data from remaining folds
        path_train = os.path.join(dir_out, 'train.arff')
        f_train = open(path_train, 'w')
        for l in header:
            f_train.write(l)
        for j, g in enumerate(folds):
            if i != j:
                for data_i in g:
                    print >> f_train, ','.join(data[data_i])
        f_train.close()


def main(argv):

    if len(argv) != 5:
        print >> sys.stderr, __doc__
        return 64

    seed = int(argv[1])
    n_folds = int(argv[2])
    path_features = argv[3]
    path_out_dir = argv[4]

    # Read in and parse feature file
    header, data = parse_arff(path_features)

    # Write out stratified folds
    # Fold will hold a list for each class in data; in each class list,
    # it will have indices of data points from `data`. 
    folds = create_folds(seed, data, n_folds)

    # Serialize folds
    serialize_folds(header, data, folds, path_out_dir)

    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))

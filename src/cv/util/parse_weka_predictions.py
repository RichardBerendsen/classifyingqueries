"""
Usage:
    parse_weka_predictions out_dir < input 

Parses weka predictions, and create two files in `out_dir`:
test.class_labels, and predictions, respectively. 
"""
import sys
import os
import re

def main(argv):

    if len(argv) != 2:
        print >> sys.stderr, __doc__
        return 64

    path_out_dir = argv[1]

    path_test_class_labels = os.path.join(path_out_dir,
            'test.class_labels')
    path_predictions = os.path.join(path_out_dir,
            'predictions')

    if os.path.exists(path_test_class_labels):
        print sys.stderr, "File exists: %s" % (path_test_class_labels,)
        return 2
    else:
        f_test_class_labels = open(path_test_class_labels, 'w')

    if os.path.exists(path_predictions):
        print sys.stderr, "File exists: %s" % (path_predictions,)
        return 2
    else:
        f_predictions = open(path_predictions, 'w')

    # Match lines matching the following pattern
    prediction_line = r'.+(\d:\S+)\s+(\d:\S+).+'

    for l in sys.stdin:
        m = re.match(prediction_line, l)
        if m is not None:
            print >> f_test_class_labels, m.group(1)
            print >> f_predictions, m.group(2)
    return 0

    f_test_class_labels.close()
    f_predictions.close()

if __name__ == '__main__':
    sys.exit(main(sys.argv))

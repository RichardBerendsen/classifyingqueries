#!/bin/bash
# Do not allow unset variables
set -o nounset

function usage {
    cat >&2 <<EOF
Usage:
    run_J48.sh seed folds_dir out_dir

Will run Weka's J48 class with default parameters (as of version 3.6.4).
Needs folds in folds_dir, assumes a format like this:

    fold_dir/1/train.arff
    fold_dir/1/test.arff
    fold_dir/2/train.arff
    fold_dir/2/test.arff
    ...

Will 'mkdir' out_dir, and in it:

    out_dir/1/predictions
    out_dir/2/predictions
    ...

That's all, folks. Do not forget to supply the seed parameter, it will
be passed on to the J48 class.
EOF
}

# Get options
while getopts "h" opt; do
  case $opt in
    h)
      usage
      exit 0
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
  esac
done
shift $(($OPTIND - 1))

# Check arguments
if [ "$#" -ne "3" ]
then
    usage
    exit 64
fi

seed="$1"
folds_dir="${2%%/}"
out_dir="${3%%/}"

# Check folds_dir exists
[ -d "$folds_dir" ] || exit "$?"

# Make outdir (exit if mkdir gives an error)
mkdir "$out_dir" || exit "$?"

for t in "$folds_dir"/*/train.arff
do

    # Some
    fold_dir="$( dirname "$t" )"
    relative_fold_dir="${fold_dir##$folds_dir}"
    mkdir "$out_dir/$relative_fold_dir" || exit "$?"

    java -cp "$( locate weka.jar )" weka.classifiers.trees.J48 -C 0.25 \
            -M 2 -Q 1 -t "$t"  -T "$fold_dir/test.arff" -p 0 \
            > "$out_dir/$relative_fold_dir/weka.out" || exit "$?"

    python "$( dirname "$0" )/util/parse_weka_predictions.py" \
            "$out_dir/$relative_fold_dir" \
            < "$out_dir/$relative_fold_dir/weka.out" \
            || exit "$?"
done

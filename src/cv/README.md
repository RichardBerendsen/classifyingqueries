Reproducing the experiments
---------------------------

In this directory you will find scripts and documentation for doing
cross validation yourself, instead of letting Weka do it for you.

First locate the feature files. These files, with the feature values we
used in our paper, can be found in `./exp` (all references to stuff in
this git repository starts from the root directory of the repository)
they have an extension .arff, the standard format of feature files for
Weka. 

Then, install Weka. We used Weka 3.6.4 to run the experiments, you can use
newer versions, of course, though the results may differ a bit (hopefully for
the better ;-)).

Okay, you're all set. Follow the steps below to create stratified folds,
run a classifier and output predictions, compute performance scores, and
perform significance tests.

### Stratified cross validation

Parse the feature files, and create ten stratified folds. Stratified
here means that each class appears in each fold with approximately the
same ratio as in the full data set. This could be done in Weka, but we
opted for writing a small script, ourselves: `./src/cv/make_folds.py`. You
can use it like so:

    python ./src/cv/make_folds.py 1 3 ./exp/high_low.arff \
            /tmp/high_low_folds

to create `3` folds, generated randomly (with the seed set to `1`),
stored in the directory `/tmp/high_low_folds`. Call the script without
arguments to get some help.

This command should create a directory structure somewhat like:

    /tmp/high_low_folds
    /tmp/high_low_folds/1
    /tmp/high_low_folds/1/test.arff
    /tmp/high_low_folds/1/train.arff
    /tmp/high_low_folds/2
    /tmp/high_low_folds/2/test.arff
    /tmp/high_low_folds/2/train.arff
    /tmp/high_low_folds/3
    /tmp/high_low_folds/3/test.arff
    /tmp/high_low_folds/3/train.arff

### Running a classifier

The next step is to train and test a classifier on each of the folds,
and to record its predictions on each test point; that file could just
have the format of a class label on each line, where the predictions are
ordered the same as the data points in the corresponding test.arff
files.

Let us work through the motions for the J48 classifier in Weka. Assuming
we are in one of the directories we just created, e.g.,
`/tmp/high_low_folds/1`, we could run the following command:

    java -cp "$( locate weka.jar )" weka.classifiers.trees.J48 -C 0.25 \
            -M 2 -Q 1 -t 'train.arff' -T 'test.arff' -p 0

The `-C`, `-M` and `-Q` (seed) options are specific for the J48 class,
and set to their default values, like we used in the paper. The `-t`
and `-T` options specify the training and test set, respectively.
Specifying `-p 0` tells the classifier to output the predictions (but no
attributes). Finally, `-i` would request detailed performance output,
but if `-p` is specified, there will only be predictions, no detailed
performance output. See also http://weka.wikispaces.com/Primer.

So, if you do not look forward to doing this by hand for each fold,
there is a bash script that will do it for you. The bash script also
takes care of parsing the output of Weka, and saves two files, called
`test.class_labels` and `predictions`. To store these files, it creates
a directory structure that mirrors the one in the `/tmp/high_low_folds`
(or where-ever you stored the folds). This way, you can run different
classifiers on the same folds, the bash script does not touch the
directory that has the folds.

You can call the bash script for J48 like this:

    ./src/cv/run_J48.sh seed folds_dir out_dir

As an aside, it uses the script `./src/cv/util/parse_weka_predictions.py`,
which parses the Weka output.

### Computing performance scores

Congratulations! You have run a classifier, and now you are ready to
compute performance scores. This could be done for each single fold, but
in this paper we chose a different setup. We will concatenate all the
predictions on all the test folds together before computing a single
performance score. We also concatenate all the test.class_labels from
all the folds (in the same order as the predictions). Then we end up
with just two files: (1) test.class_labels, and (2) predictions. That
will be enough to compute all the scores we want. The following Python
script will take care of this:

    python ./src/cv/concat_folds.py predictions_dir out_dir

`concat_folds.py` will create out_dir for you and put the two files in
there. 

So now, we can compute performance scores. Let's compute for each class
the metrics precision, recall, and the harmonic mean between these two
(also called the F-measure, where precision and recall are equally
weighted). In addition, we will compute a single metric for a run, a
macro-averaged F-measure. There are different definitions of this
metric. One is to just average the F-measures for each class. Another
is to use as precision and recall the average precision and the average
recall over classes, respectively. The latter definition is from the
paper:

Sokolova, M., & Lapalme, G. (2009). A systematic analysis of performance
measures for classification tasks. Information Processing & Management,
45(4), 427-437.

and this is the definition we use. The command

    python ./src/cv/compute_metrics.py test.class_labels predictions 

This will compute the metrics and print them to `stdout` (Note that the
script will also work with the test.class_labels and predictions of a
single fold). That will give you the performance scores for a single
classifier on either the binary or the three-way classification task. In
the next step, we will talk about how to compute a signficance test
between the scores for two classifiers on the same task.

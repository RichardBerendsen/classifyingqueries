"""
Usage:
    parse_weka_predictions < input 

Parse Weka predictions, and output for each instance its' ID, actual and
predicted label, seperated by tabs.
"""
import sys
import os
import re

def main(argv):

    if len(argv) != 1:
        print >> sys.stderr, __doc__
        return 64

    # Match lines matching the following pattern
    prediction_line = r'.+(\d:\S+)\s+(\d:\S+).+\((\S+)\)'

    for l in sys.stdin:
        m = re.match(prediction_line, l)
        if m is not None:
            id = m.group(3)
            truth = m.group(1)
            predicted = m.group(2)
            print '%s\t%s\t%s' % (id, truth, predicted)
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))

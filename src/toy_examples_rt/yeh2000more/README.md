# Toy example for the randomization test from the literature

From the paper

Yeh, A. (2000, July). More accurate tests for the statistical significance of
result differences. In Proceedings of the 18th conference on Computational
linguistics-Volume 2 (pp. 947-953). Association for Computational Linguistics.

In Section 3.3, an example is worked out for classification metrics. The test
used there is one-tailed, so running

    python ../../rt.py method_1 method_2 100000 some_tmp_dir

should print p-values about twice as large as those mentioned in the paper.
When I ran this, I got

    $ python ../../rt.py method_1 method_2 100000 tmp
    P-0: 0.000220
    P-1: 0.040270
    R-0: 0.000000
    R-1: 0.000180
    F-0: 0.000020
    F-1: 0.029490
    Avg-P: 0.004500
    Avg-R: 0.005200
    Avg-F: 0.069950
    F_M: 0.004950

Note that we are looking for `P-1`, `R-1` and `F-1` here (precision, recall and
f-score for the class with label `1`. The p-values for `R-1` and `F-1` are
correct, the p-value for `P-1` is off a bit: I would have expected it to be
close to 0.50 there. This is a bit odd: adding more toy examples from the
literature could shed more light on this discrepancy.


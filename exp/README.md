Experimental Data
=================

This directory contains data you need to perform experiments.

Currently, it contains just the feature files for two classification
experiments:

1. `high_low.arff`: Binary classification into high-profile and
low-profile queries.

2. `three_way.arff`: Classification into low-profile, regular
high-profile and event-based high profile queries.
